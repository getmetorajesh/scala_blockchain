package blockchain

import java.time.Instant

case class Block(
                Index: Int,
                Timestamp: Instant,
                BPM: Int,
                Hash: String,
                PrevHash: String
                )