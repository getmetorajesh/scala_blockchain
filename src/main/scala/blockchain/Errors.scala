package blockchain

import cats.implicits._
object Errors {
  type ErrorOr[A] = Either[String, A]
  def toFailure[A](e: String): ErrorOr[A] = Either.left(e)
  def toSuccess[A](a: A): ErrorOr[A] = Either.right(a)
}