package blockchain
import java.time.Instant
import Errors._
import cats.implicits._
import cats.data.Kleisli


/**
  */

case class RawBlock(index: Int, time: Instant, BPM: Int, prevHash: String)

class Main {
  type BlockChain = List[Block]

  val graph = (Kleisli(validateInput) andThen
    calculateHash).run

  graph((1, List()))
//      generateBlock andThen
//        validateBlock andThen
//          appendBlock

  def validateInput: ((Int, BlockChain)) => ErrorOr[RawBlock] = { case (bpm, blockChain) =>
    val raw = new RawBlock(
      blockChain.last.Index,
      Instant.now(),
      bpm,
      blockChain.last.Hash
    )
    Right(raw)
  }


  def calculateHash: RawBlock => ErrorOr[String] = { (raw) =>
    Right("")
  }

  def generateBlock(oldBlock: Block, BPM: Int): ErrorOr[Block] = {
    val newIndex = oldBlock.Index + 1
    val time = Instant.now()
    val newBlock = new Block(
      newIndex,
      time,
      BPM,
      oldBlock.Hash,
      //calcualateHash(newIndex, time, BPM, oldBlock.Hash)
    )
    Right(newBlock)
  }

}
